<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Aprenda a trabalhar com filas no Laravel
>  Entenda como funcionam filas no Laravel e como você pode se beneficiar delas para deixar a sua aplicação mais performática.

```bash
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate --seed
php artisan horizon
```
- http://localhost:8000/send-notification-to-all
- http://localhost:8000/run-batch
- http://localhost:8000/horizon/dashboard
- http://localhost:8000/teste
- http://localhost:8000/send

`php artisan queue:work --timeout=60` 
OR 
`php artisan queue:work --tries=2 `

`❯ php artisan cache:clear && php artisan config:cache && php artisan config:clear && composer dump-autoload -o`
`❯ php artisan horizon:snapshot`

## Video explicativo de Pinguim do Laravel · Rafael Lunardelli

- https://www.youtube.com/watch?v=fADU1GFbjwM

- Renato Lucena 2022