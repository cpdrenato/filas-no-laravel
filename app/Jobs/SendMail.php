<?php

namespace App\Jobs;

use App\Models\User;
use App\Mail\SendMailUser;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\Middleware\RateLimited;
use Throwable;

class SendMail implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public int $tries = 5;
    //public $user;

    /**
     * Create a new message instance.
     * @param User $user
     *
     * @return void
     */
    public function __construct()
    {
        //$this->user = $user;
    }

    public function middleware()
    {
        return [
            new RateLimited('notifications')
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Precisa importar a classe Mail:
        // use Illuminate\Support\Facades\Mail;
        //Mail::to($to)->send(new SendMailUser($user));
        $id = 1;
        $user = User::where('id', $id)->first();

        Mail::to($user->email)->send(new SendMailUser($user));
    }

    public function failed(Throwable $exception)
    {
        // Ao falhar enviar email.
        \Log::error('falha neste job');
    }
}
