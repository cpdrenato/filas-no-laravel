<?php

namespace App\Http\Controllers;

use App\Jobs\SendMail;
use App\Models\User;
use App\Mail\SendMailUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function mail()
    {
        $id = 1;
        $user = User::where('id', $id)->first();
        
        Mail::to($user->email)->send(new SendMailUser($user));
        echo 'ok!';
    }
}
